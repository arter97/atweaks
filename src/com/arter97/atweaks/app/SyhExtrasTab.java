package com.arter97.atweaks.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class SyhExtrasTab extends SyhTab implements OnClickListener {
     public SyhExtrasTab(Context context, Activity activity) {
		super(context, activity);
		this.name = "Extras";
	}

	@Override
	public View getCustomView(ViewGroup parent)
	{
 		 View v = LayoutInflater.from(mContext).inflate(R.layout.syh_extrastab, parent, false);

         final Button button4 = (Button) v.findViewById(R.id.ResetSettings);
         button4.setOnClickListener(this);

         final Button button5 = (Button) v.findViewById(R.id.Boeffla);
         button5.setOnClickListener(this);

         String s = "";
    	 s += "\n\nKernel version : " + System.getProperty("os.version");
         final TextView tv = (TextView) v.findViewById(R.id.textViewAppVersion);
         try
         {
             final String appVersion = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName;
             s += ("\naTweaks version : " + appVersion);
         }
         catch (NameNotFoundException e)
         {
         }

         final TextView tv2 = (TextView) v.findViewById(R.id.textViewKernelVersion);
         tv2.setText(s);

 		 return v;
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
            case R.id.Boeffla:
                Context objContext;
                objContext= mContext;
                Intent i = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.near.boefflasound"));
                objContext.startActivity(i);
                break;
            case R.id.ResetSettings:
            	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        	    builder.setMessage(R.string.reset_all)
        	           .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	                  // Handle Ok
                                Utils.executeRootCommandInThread("/res/arter97.sh delete default");
                                Utils.executeRootCommandInThread("rm /data/.arter97/customconfig.xml");
                                Utils.executeRootCommandInThread("/res/arter97.sh apply");
                                Utils.executeRootCommandInThread("/res/busybox sh -c 'am force-stop com.arter97.atweaks.app && am start com.arter97.atweaks.app/.MainActivity'");
        	           	    	System.exit(0);
        	              }
        	          })
        	          .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	               // Handle Cancel
        	           }
        	          })
        	          .setTitle(R.string.warning)
        	          //.setIcon(R.drawable.ic_launcher)
        	          .create();
        	    builder.show();

        	break;
		}		
	}
}
